using UnityEngine;
using UnityEngine.Events;
using System.Collections;

/// <summary>
/// Clase que representa al jugador, su movimiento, acciones y eventos asociados
/// @author Ra�l �ngel Lago Castro
/// </summary>
[RequireComponent(typeof(Rigidbody), typeof(CapsuleCollider))]
public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float      speed             = 6f;
    [SerializeField]
    private float      jumpStrength      = 15f;
    [SerializeField]
    private LayerMask  groundMask        = 0;
    [SerializeField]
    private float      groundCheckRadius = 0.3f;
    [SerializeField]
    private float      airSlow           = 0.6f;
    [SerializeField]
    private float      sprintMul         = 2f;
    [SerializeField]
    private Transform  mrParentTranform  = null;
    [SerializeField]
    private float      mrScaleFactor     = 0.7f;
    [SerializeField]
    private float      crouchYLocalPos   = 1f;

    public Rigidbody rb { get; private set; }
    private CapsuleCollider         cc;
    private DefaultInputActionAsset iaa;

    private Vector2 velocity;
    private bool    grounded = false;
    private bool    crouched = false;
    private float   defaultYCamera;
    private float   defaultYCollider;
    private bool    sprint = false;
    private Vector3 lastTimeFloor;

    [SerializeField]
    private UnityEvent OnShoot = null;

    private class PlayerData
    {
        public Vector3    linearVelocity;
        public Vector3    angularVelocity;
        public Vector3    lastTimeFloor;
        public Vector3    savedPos;
        public Quaternion savedRot;

        public PlayerData(PlayerController pd)
        {
            linearVelocity  = pd.rb.velocity;
            angularVelocity = pd.rb.angularVelocity;
            lastTimeFloor   = pd.lastTimeFloor;
            savedPos        = pd.transform.position;
            savedRot        = pd.transform.rotation;

        }
    }

    private PlayerData pd;


    //---------------------------------------------------------------

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        cc = GetComponent<CapsuleCollider>();

        defaultYCamera   = Camera.main.transform.localPosition.y;
        defaultYCollider = cc.height;

        iaa = new DefaultInputActionAsset();

        iaa.Gameplay.Crouch        .performed += _ => Crouch();
        iaa.Gameplay.Jump          .performed += _ => Jump();
        iaa.Gameplay.Shoot         .performed += _ => Shoot();
        iaa.Gameplay.ImpulseTerrain.performed += _ => Shoot();
        iaa.Gameplay.Sprint        .performed += _ => Sprint();
    }

    void OnEnable()
    {
        iaa.Enable();
    }

    void OnDisable()
    {
        iaa.Disable();
    }

    void Update()
    {
        //Comprobar si el jugador est� tocando el suelo
        grounded = Physics.CheckSphere(transform.position, groundCheckRadius, groundMask);

        //MOVIMIENTO

        velocity = iaa.Gameplay.Move.ReadValue<Vector2>();

        //Clamp en vez de normalizar para contemplar cuando la magnitud del vector es menor que 1 al empezar a moverse
        velocity = Vector2.ClampMagnitude(velocity, 1) * speed;

        if (!grounded)
        {
            velocity = velocity * airSlow;
        }

        if (sprint)
        {
            velocity *= sprintMul;
        }

        rb.velocity = transform.forward * velocity.y +
                      transform.right   * velocity.x +
                      transform.up      * rb.velocity.y;

        if (rb.velocity.magnitude <= 0.5f) sprint = false;
    }

    private void Sprint()
    {
        sprint = !sprint;
    }

    private void Jump()
    {
        if (grounded)
        {
            //rb.AddForce(Vector3.up * jumpStrength, ForceMode.Impulse);
            rb.velocity = new Vector3(rb.velocity.x, jumpStrength, rb.velocity.z);
        }
    }

    private void Crouch()
    {
        if (!crouched)
        {
            // Ajustamos la posici�n local de la c�mara
            Camera.main.transform.localPosition = new Vector3
            (
                Camera.main.transform.localPosition.x,
                crouchYLocalPos,
                Camera.main.transform.localPosition.z
            );

            //Escalamos el objeto padre del mesh renderer que al estar colocado en el suelo va a afectar solo a la altura
            mrParentTranform.localScale = new Vector3(1f, mrScaleFactor, 1f);

            //Ajustamos la altura el collider y el centro del mismo para bajarlo al suelo
            cc.height = defaultYCollider - (defaultYCamera - crouchYLocalPos);
            cc.center = Vector3.up * cc.height * 0.5f; //el centro siempre est� a la mitad de su altura en distancia en "y" del objecto padre
        }
        else
        {
            // Ajustamos la posici�n local de la c�mara
            Camera.main.transform.localPosition = new Vector3
            (
                Camera.main.transform.localPosition.x,
                defaultYCamera,
                Camera.main.transform.localPosition.z
            );

            //Restablecemos la escala del padre del mesh renderer a sus valores por defecto
            mrParentTranform.localScale = Vector3.one;

            //Ajustamos la altura el collider y el centro del mismo para bajarlo al suelo
            cc.height = defaultYCollider;
            cc.center = Vector3.up * cc.height * 0.5f; //el centro siempre est� a la mitad de su altura en distancia en "y" del objecto padre
        }

        crouched = !crouched;
    }

    private void Shoot()
    {
        OnShoot.Invoke();
    }

    private void OnCollisionEnter(Collision collision)
    {
        //layer 8 (1<<8) ->  00000000000000000000000010000000
        //groundmask     ->  00000000000000000000000010000000
        //&              ->  00000000000000000000000010000000 != 0
        if (((1 << collision.gameObject.layer) & groundMask) != 0)
        {
            RaycastHit hit;
            if
            (
                grounded &&
                Physics.Raycast(transform.position + new Vector3(0f, 0.1f, 0f), Vector3.down, out hit, 1, groundMask)
            )
            {
                lastTimeFloor = hit.point;
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        //layer 8 (1<<8) ->  00000000000000000000000010000000
        //groundmask     ->  00000000000000000000000010000000
        //&              ->  00000000000000000000000010000000 != 0
        if (((1<<collision.gameObject.layer) & groundMask) != 0)
        {
            RaycastHit hit;
            if
            (
                grounded &&
                Physics.Raycast(transform.position, Vector3.down, out hit, 1, groundMask)
            )
            {
                lastTimeFloor = hit.point;
            }
        }
    }

    public void FallRecovery()
    {
        RaycastHit hit;
        if(Physics.Raycast(lastTimeFloor + new Vector3(0,1,0), Vector3.down, out hit, 2, groundMask))
        {
            transform.position = lastTimeFloor;
        }
        else
        {
            transform.position = Vector3.zero;
        }

        rb.velocity = Vector3.zero;
    }

    public void SaveState()
    {
        pd = new PlayerData(this);
        ObjectPooler.instance.SpawnFromPool("PlayerBackup", transform.position, transform.rotation);
    }

    public void LoadState(float delay)
    {
        if (pd == null) return;

        StopCoroutine(nameof(LoadAfterTime));
        StartCoroutine(LoadAfterTime(delay));
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, groundCheckRadius);
    }

    private IEnumerator LoadAfterTime(float time)
    {
        yield return new WaitForSeconds(time);

        rb.velocity = pd.linearVelocity;
        rb.angularVelocity = pd.angularVelocity;
        lastTimeFloor = pd.lastTimeFloor;
        transform.position = pd.savedPos;
        transform.rotation = pd.savedRot;
    }
}
