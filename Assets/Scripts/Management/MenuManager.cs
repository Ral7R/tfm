using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

/// <summary>
/// Script para gestionar todas la funcionalidades del men� incial y los ajustes
/// @author Ra�l �ngel Lago Castro
/// </summary>
public class MenuManager : MonoBehaviour
{
    [SerializeField]
    private InputField inpFldSmoothing;
    [SerializeField]
    private InputField inpFldSensitivity;

    [SerializeField]
    private AudioMixer mixer;

    [SerializeField]
    private Slider sliderMaster;
    [SerializeField]
    private Slider sliderMusic;
    [SerializeField]
    private Slider sliderFX;
    [SerializeField]
    private Text txtSmoothing;
    [SerializeField]
    private Text txtSensitivity;
    [SerializeField]
    private Toggle myToggle;

    [SerializeField]
    private GameObject canvasMain;
    [SerializeField]
    private GameObject canvasSettings;

    [SerializeField]
    private Button btnSettings;
    [SerializeField]
    private Button btnApply;
    [SerializeField]
    private Button btnBack;
    [SerializeField]
    private Button btnPlay;
    [SerializeField]
    private Button btnQuit;

    [SerializeField]
    private SO_Settings settings;

    [SerializeField]
    private float delayNextScene = 1f;

    void Awake()
    {
        inpFldSmoothing.  characterValidation = InputField.CharacterValidation.Decimal;
        inpFldSensitivity.characterValidation = InputField.CharacterValidation.Decimal;

        btnApply.onClick.AddListener
        (
            delegate ()
            {
                OnApply
                    (
                        myToggle.isOn,
                        float.Parse(txtSmoothing.text.  Replace(".", ",")),
                        float.Parse(txtSensitivity.text.Replace(".", ","))
                    );
            }
        );

        btnSettings.onClick.AddListener
        (
            delegate ()
            {
                OnSettings();
            }
        );

        btnBack.onClick.AddListener
        (
            delegate ()
            {
                OnBack();
            }
        );

        btnPlay.onClick.AddListener
        (
            delegate ()
            {
                OnPlay();
            }
        );

        btnQuit.onClick.AddListener
        (
            delegate ()
            {
                OnQuit();
            }
        );
    }

    void Start()
    {
        canvasMain.    SetActive(true);
        canvasSettings.SetActive(false);

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        mixer.SetFloat("masterVolume", settings.masterVolume - 80f);
        mixer.SetFloat("musicVolume",  settings.musicVolume - 80f);
        mixer.SetFloat("fxVolume",     settings.fxVolume - 80f);
    }

    private void OnSettings()
    {
        canvasMain.    SetActive(false);
        canvasSettings.SetActive(true);

        myToggle.isOn          = settings.enabledSmoothing;
        inpFldSmoothing.text   = settings.smoothingValue.ToString();
        inpFldSensitivity.text = settings.sensitivity.ToString();

        float value;

        if (mixer.GetFloat("masterVolume", out value))
        {
            sliderMaster.value = value + 80f;
        }

        if (mixer.GetFloat("musicVolume", out value))
        {
            sliderMusic.value = value + 80f;
        }

        if (mixer.GetFloat("fxVolume", out value))
        {
            sliderFX.value = value + 80f;
        }
    }

    private void OnApply(bool bToggle, float valueSm, float valueSe)
    {
        settings.enabledSmoothing = bToggle;
        settings.smoothingValue   = valueSm;
        settings.sensitivity      = valueSe;

        settings.masterVolume = sliderMaster.value;
        settings.musicVolume  = sliderMusic.value;
        settings.fxVolume     = sliderFX.value;

        canvasMain.    SetActive(true);
        canvasSettings.SetActive(false);

        mixer.SetFloat("masterVolume", sliderMaster.value - 80f);
        mixer.SetFloat("musicVolume",  sliderMusic.value - 80f);
        mixer.SetFloat("fxVolume",     sliderFX.value - 80f);
    }

    private void OnBack()
    {
        canvasMain.    SetActive(true);
        canvasSettings.SetActive(false);
    }

    private void OnPlay()
    {
        StartCoroutine(LoadSceneAfterSeconds(1, delayNextScene));
    }

    private IEnumerator LoadSceneAfterSeconds(int index, float seconds)
    {
        yield return new WaitForSeconds(seconds);
        SceneManager.LoadScene(index);
    }

    private void OnQuit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
