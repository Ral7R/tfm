using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script que restablece la posici�n de un objeto interactivo al entrar en un trigger
/// @author Ra�l �ngel Lago Castro
/// </summary>
public class OnInteractiveTrigger : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        IInteractable interactable = other.transform.GetComponent<IInteractable>();
        if (interactable != null) interactable.ResetPos();
    }
}
