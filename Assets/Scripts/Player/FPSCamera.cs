using UnityEngine;

/// <summary>
/// Script que mueve la c�mara de acuerdo a los ajustes y como esta afecta al movimiento del jugador
/// @author Ra�l �ngel Lago Castro
/// </summary>
public class FPSCamera : MonoBehaviour
{
    private Transform cameraTrm;
    private Vector2   mouseLook;
    private Vector2   mouseDelta;
    private Vector2   smoothMouseDelta;
    private DefaultInputActionAsset iaa;

    [SerializeField]
    private SO_Settings settings;

    void Awake()
    {
        cameraTrm = Camera.main.transform;
        iaa = new DefaultInputActionAsset();
    }

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible   = false;
    }

    void OnEnable()
    {
        iaa.Enable();
    }

    void OnDisable()
    {
        iaa.Disable();
    }

    void Update()
    {
        mouseDelta = iaa.Gameplay.Camera.ReadValue<Vector2>();

        if (settings.enabledSmoothing)
        {
            // Obtenemos la delta a la que vamos a transicionar para crear el efecto de "Smoothing".
            // Con la sesibilidad exageramos el valor de los ejes del movimiento del rat�n cuando smoothing sea positivo.
            smoothMouseDelta = new Vector2(mouseDelta.x * (settings.sensitivity * settings.smoothingValue * Time.deltaTime),
                                           mouseDelta.y * (settings.sensitivity * settings.smoothingValue * Time.deltaTime));

            // Se establece la nueva delta del rat�n interpolando entre la anterior y la delta con "smooth".
            // El valor t del Lerp se establece como el inverso del valor de smooth,
            // a m�s smoothingValue m�s cerca estaremos de la delta original y m�s tarda la c�mara en seguir al rat�n.
            mouseDelta = Vector2.Lerp(mouseDelta, smoothMouseDelta, 1 / settings.smoothingValue);
        }
        else
        {
            //Delta sin smoothing, usamos el valor directamente
            mouseDelta = new Vector2(mouseDelta.x * (settings.sensitivity * Time.deltaTime),
                                     mouseDelta.y * (settings.sensitivity * Time.deltaTime));
        }

        //Agregamos la delta
        mouseLook += mouseDelta;

        // Restringimos el movimiento en el eje vertical para solo mirar "hacia delante".
        mouseLook.y = Mathf.Clamp(mouseLook.y, -90, 90);

        // Rotamos la c�mara en vertical y al jugador en horizontal,
        // para que el movimiento hacia delante sea siempre hacia donde mira la c�mara.
        transform.localRotation = Quaternion.AngleAxis(mouseLook.x, Vector3.up);
        cameraTrm.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
    }
}
