#if UNITY_EDITOR
    using UnityEngine;
    using UnityEditor;

    /// <summary>
    /// Extensi�n del editor de Unity para agragar elementos personalizados al inspector
    /// @author Ra�l �ngel Lago Castro
    /// </summary>
    [CustomEditor(typeof(SO_GameEvent))]
    public class CustomInsp : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            SO_GameEvent myCustomGE = (SO_GameEvent)target;
            
            //Agrega un bot�n para activar los eventos desde el inspector
            if (GUILayout.Button("Raise()"))
            {
                myCustomGE.Raise();
            }
        }
    }
#endif