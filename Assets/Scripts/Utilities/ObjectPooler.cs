using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase para preinstaciar y gestionar objetos para evitar instanciaci�n en tiempo de ejecuci�n
/// @author Ra�l �ngel Lago Castro
/// </summary>
public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler instance { get; private set; }

    [System.Serializable]
    private class Pool
    {
        public string tag = null;
        public GameObject prefab = null;
        public int size = 0;
    }

    [SerializeField]
    private List<Pool> pools = null;

    private Dictionary<string, Queue<GameObject>> poolDictionary;

    void Awake()
    {
        instance = this; //no se crea un Singleton real para no complicar la implementaci�n del pooler
                         //respecto al resto de elementos del juego

        poolDictionary = new Dictionary<string, Queue<GameObject>>();

        foreach (Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();

            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }

            poolDictionary.Add(pool.tag, objectPool);
        }
    }

    public GameObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation)
    {
        if (!poolDictionary.ContainsKey(tag)) return null;

        GameObject objectToSpawn = poolDictionary[tag].Dequeue();

        objectToSpawn.SetActive(false);
        objectToSpawn.SetActive(true);
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;

        poolDictionary[tag].Enqueue(objectToSpawn);

        return objectToSpawn;
    }
}
