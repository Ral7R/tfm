using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Scriptable Object para representar eventos que no generan dependencia entre scripts ni gameobjects
/// @author Ra�l �ngel Lago Castro
/// </summary>
[CreateAssetMenu(fileName = "New GameEvent", menuName = "SO/GameEvent")]
public class SO_GameEvent : ScriptableObject
{
	private List<GameEventListener> listeners =
		new List<GameEventListener>();

	//Se llama a todos las funciones asignadas como respuesta en todos los
	//GameEventListeners "suscritos" al evento que representa el Scriptable Object
	public void Raise()
	{
		for (int i = listeners.Count - 1; i >= 0; i--)
			listeners[i].OnEventRaised();
	}

	public void RegisterListener(GameEventListener listener)
	{
		listeners.Add(listener);
	}

	public void UnregisterListener(GameEventListener listener)
	{
		listeners.Remove(listener);
	}
}
