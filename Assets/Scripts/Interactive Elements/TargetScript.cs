using UnityEngine;

/// <summary>
/// Clase para las dianas que activan el evento de golpeo de una diana
/// @author Ra�l �ngel Lago Castro
/// </summary>
public class TargetScript : MonoBehaviour, IInteractable
{
    [SerializeField]
    private SO_GameEvent OnHit;

    public void Interact()
    {
        //soScore.AddScore(speed);
        OnHit.Raise();
        Destroy(gameObject);
    }

    public void ResetPos()
    {
        
    }
}
