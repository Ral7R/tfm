/// <summary>
/// Interfaz para todos los objetos con los que se puede interactuar
/// @author Ra�l �ngel Lago Castro
/// </summary>
public interface IInteractable
{
    void Interact();

    void ResetPos();
}
