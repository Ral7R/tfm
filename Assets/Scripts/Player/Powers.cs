using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase para gestionar los diferentes poderes del jugador y activar los
/// eventos asociados de acuerdo a su l�gica
/// @author Ra�l �ngel Lago Castro
/// </summary>
public class Powers : MonoBehaviour
{
    private DefaultInputActionAsset iaa;

    [SerializeField]
    private SO_GameEvent[] powers    = null;
    [SerializeField]
    private LayerMask ImpulseTerrain = 0;
    //[SerializeField]
    //private GameObject ImpulseGO     = null;

    [SerializeField]
    private float impTerDistance     = 10f;
    [SerializeField]
    private float shootDistance      = 50f;
    [SerializeField]
    private float timeToClearLaser   = 0.5f;
    [SerializeField]
    private Transform shootPoint     = null;
    [SerializeField]
    private LineRenderer lr;
    [SerializeField]
    private LineRenderer lrSec;

    // Start is called before the first frame update
    void Awake()
    {
        iaa = new DefaultInputActionAsset();

        iaa.Gameplay.ImpulseTerrain.performed += _ => PlaceImpulse();
        iaa.Gameplay.Shoot         .performed += _ => Shoot();
        iaa.Gameplay.SaveSS        .performed += _ => UsePower(0);
        iaa.Gameplay.LoadSS        .performed += _ => UsePower(1);
        iaa.Gameplay.SavePS        .performed += _ => UsePower(2);
        iaa.Gameplay.LoadPS        .performed += _ => UsePower(3);
        

        lr.enabled    = false;
        lrSec.enabled = false;
    }

    void OnEnable()
    {
        iaa.Enable();
    }

    void OnDisable()
    {
        iaa.Disable();
    }

    private void UsePower(int index)
    {
        powers[index].Raise();
    }

    private void Shoot()
    {
        StopCoroutine(nameof(DisableLR));

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, shootDistance, ImpulseTerrain))
        {
            lr.enabled = true;
            lr.SetPosition(0, shootPoint.position);
            lr.SetPosition(1, hit.point);

            IInteractable interactable = hit.transform.GetComponent<IInteractable>();
            if (interactable != null) interactable.Interact();
        }
        else
        {
            lr.enabled = true;
            lr.SetPosition(0, shootPoint.position);
            lr.SetPosition(1, Camera.main.transform.position + (Camera.main.transform.forward * shootDistance));
        }

        StartCoroutine(nameof(DisableLR));
    }

    private void PlaceImpulse()
    {
        StartCoroutine(nameof(DisableSecLR));

        RaycastHit hit;
        if(Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, impTerDistance, ImpulseTerrain))
        {
            lrSec.enabled = true;
            lrSec.SetPosition(0, shootPoint.position);
            lrSec.SetPosition(1, hit.point);

            //Instantiate(ImpulseGO, hit.point, Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0));
            ObjectPooler.instance.SpawnFromPool("Impulse", hit.point, Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0));
        }
        else
        {
            lrSec.enabled = true;
            lrSec.SetPosition(0, shootPoint.position);
            lrSec.SetPosition(1, Camera.main.transform.position + (Camera.main.transform.forward * impTerDistance));
        }

        StartCoroutine(nameof(DisableSecLR));
    }

    private IEnumerator DisableLR()
    {
        yield return new WaitForSeconds(timeToClearLaser);
        lr.enabled = false;
    }

    private IEnumerator DisableSecLR()
    {
        yield return new WaitForSeconds(timeToClearLaser);
        lrSec.enabled = false;
    }
}
