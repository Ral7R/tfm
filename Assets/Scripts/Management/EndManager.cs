using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Script para gestionar la pantalla de final de partida
/// @author Ra�l �ngel Lago Castro
/// </summary>
public class EndManager : MonoBehaviour
{
    [SerializeField]
    private Button btnQuit;
    [SerializeField]
    private Button btnContinue;

    void Awake()
    {
        btnQuit.onClick.AddListener
        (
            delegate ()
            {
                OnQuit();
            }
        );

        btnContinue.onClick.AddListener
        (
            delegate ()
            {
                OnContinue();
            }
        );
    }

    void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    private void OnQuit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    private void OnContinue()
    {
        SceneManager.LoadScene(0);
    }
}
