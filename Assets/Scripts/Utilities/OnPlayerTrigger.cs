using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Script que activa una respuesta establecida por el editor cuando el jugador entra en un trigger
/// @author Ra�l �ngel Lago Castro
/// </summary>
public class OnPlayerTrigger : MonoBehaviour
{
    [SerializeField]
    private bool once = false;

    private bool able = true;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && able)
        {
            ExecuteEvents.Execute(this.gameObject, new BaseEventData(EventSystem.current), ExecuteEvents.submitHandler);

            if(once)
                able = false;
        }
    }
}
