using UnityEngine;

/// <summary>
/// Clase para almacenar la informaci�n de los objetos afectados por las mec�nicas de
/// manipulaci�n temporal
/// @author Ra�l �ngel Lago Castro
/// </summary>
[System.Serializable]
public class AffectedByTimeData
{
    public bool       stopped;
    public Vector3    linearVelocity;
    public Vector3    angularVelocity;
    public Vector3    lastLinearVelocity;
    public Vector3    lastAngularVelocity;
    public Vector3    savedPos;
    public Quaternion savedRot;

    public AffectedByTimeData(AffectedByTime abt)
    {
        stopped             = abt.stopped;
        linearVelocity      = abt.rb.velocity;
        angularVelocity     = abt.rb.angularVelocity;
        lastLinearVelocity  = abt.lastLinearVelocity;
        lastAngularVelocity = abt.lastAngularVelocity;
        savedPos            = abt.transform.position;
        savedRot            = abt.transform.rotation;

    }

}

/// <summary>
/// Clase que engloba los comportamientos base de los obejtos afectados
/// por las mec�nicas de manipulaci�n temporal
/// @author Ra�l �ngel Lago Castro
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class AffectedByTime : MonoBehaviour, IInteractable
{
    public Rigidbody rb { get; private set; }
    public bool stopped { get; private set; } = false;
    public Vector3 lastLinearVelocity  { get; private set; }
    public Vector3 lastAngularVelocity { get; private set; }

    private bool    defKinematic;
    private bool    defUseGrvity;

    private AffectedByTimeData data = null;
    private Vector3    initPos;
    private Quaternion initRot;

    protected virtual void Awake()
    {
        rb = GetComponent<Rigidbody>();
        defKinematic = rb.isKinematic;
        defUseGrvity = rb.useGravity;
        initPos = transform.position;
        initRot = transform.rotation;
    }

    private void Affected()
    {
        if(stopped)
        {
            rb.isKinematic      = defKinematic;
            rb.useGravity       = defUseGrvity;
            rb.WakeUp();
            rb.velocity         = lastLinearVelocity;
            rb.angularVelocity  = lastAngularVelocity;
            stopped             = false;
        }
        else
        {
            lastLinearVelocity  = rb.velocity;
            lastAngularVelocity = rb.angularVelocity;
            stopped             = true;
            rb.isKinematic      = true;
            rb.useGravity       = false;
        }
    }

    public virtual void Interact()
    {
        Affected();
    }

    public virtual void SaveState()
    {
        data = new AffectedByTimeData(this);
    }

    public virtual void LoadState()
    {
        if (data == null) return;

        stopped = data.stopped;

        if (stopped)
        {
            lastLinearVelocity = rb.velocity;
            lastAngularVelocity = rb.angularVelocity;
            rb.isKinematic = true;
            rb.useGravity = false;
        }
        else
        {
            rb.isKinematic = defKinematic;
            rb.useGravity = defUseGrvity;
            rb.WakeUp();
            rb.velocity = data.linearVelocity;
            rb.angularVelocity = data.angularVelocity;
        }

        lastLinearVelocity  = data.lastLinearVelocity;
        lastAngularVelocity = data.lastAngularVelocity;
        transform.position  = data.savedPos;
        transform.rotation  = data.savedRot;
    }

    public void ResetPos()
    {
        transform.position = initPos;
        transform.rotation = initRot;
        rb.velocity        = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }
}
