using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script para desactivar objetos tras un periodo de tiempo. Complementa
/// la implementaci�n del ObjectPooler
/// @author Ra�l �ngel Lago Castro
/// </summary>
public class DisableAfterTime : MonoBehaviour
{
    public float timeToDisable = 7f;
    void OnEnable()
    {
        StopCoroutine(nameof(DisableAT));
        StartCoroutine(nameof(DisableAT));
    }

    private IEnumerator DisableAT()
    {
        yield return new WaitForSeconds(timeToDisable);
        gameObject.SetActive(false);
    }
}
