using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script para reproducir sonidos de manera normal y en reverso
/// @author Ra�l �ngel Lago Castro
/// </summary>
public class PlayingSoundsInReverse : MonoBehaviour
{
    [SerializeField]
    private AudioSource audioSource;

    public void PlayAudio(AudioClip audioClip)
    {
        StopCoroutine(nameof(StopLoop));
        audioSource.loop = false;

        audioSource.pitch = 1;
        audioSource.clip = audioClip;
        audioSource.Play();

    }

    public void PlayReverseAudio(AudioClip audioClip)
    {
        StopCoroutine(nameof(StopLoop));
        audioSource.loop = false;

        audioSource.pitch = -1;
        audioSource.loop = true;
        audioSource.clip = audioClip;
        audioSource.Play();

        StartCoroutine(StopLoop(audioClip.length * 0.5f));
    }

    private IEnumerator StopLoop(float time)
    {
        yield return new WaitForSeconds(time);
        audioSource.loop = false;
    }
}
