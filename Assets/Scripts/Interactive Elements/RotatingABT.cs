using UnityEngine;

/// <summary>
/// Clase para los objetos afectados por las mec�nicas de manipulaci�n temporal
/// y que incluye comportamineto de rotaci�n en todos los ejes
/// @author Ra�l �ngel Lago Castro
/// </summary>
public class RotatingABT : AffectedByTime
{
    [SerializeField]
    private float xRotVelocity = 0f;
    [SerializeField]
    private float yRotVelocity = 0f;
    [SerializeField]
    private float zRotVelocity = 0f;

    protected override void Awake()
    {
        base.Awake();
    }

    void Update()
    {
        if (stopped) return;

        transform.Rotate
        (
            xRotVelocity * Time.deltaTime,
            yRotVelocity * Time.deltaTime,
            zRotVelocity * Time.deltaTime
        );
    }

    public override void Interact()
    {
        base.Interact();
    }

    public override void SaveState()
    {
        base.SaveState();
    }

    public override void LoadState()
    {
        base.LoadState();
    }
}
