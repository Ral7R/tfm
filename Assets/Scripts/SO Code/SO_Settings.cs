using UnityEngine;

/// <summary>
/// Scriptable Object para representar los ajustes de control del rat�n y modificarlos desde el juego
/// @author Ra�l �ngel Lago Castro
/// </summary>
[CreateAssetMenu(fileName = "New Settings", menuName = "SO/Settings")]
public class SO_Settings : ScriptableObject
{
    public float masterVolume = 100f;
    public float musicVolume  = 100f;
    public float fxVolume     = 100f;

    public bool enabledSmoothing;

    public float smoothingValue = 1;
    public float sensitivity    = 2;
}
