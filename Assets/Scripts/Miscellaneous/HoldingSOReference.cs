using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script para mantener la referencia de Scriptable Objects y no se reseteen los valores en escenas en las que no se usa
/// @author Ra�l �ngel Lago Castro
/// </summary>
public class HoldingSOReference : MonoBehaviour
{
    [SerializeField]
    private SO_Settings settings;
}
