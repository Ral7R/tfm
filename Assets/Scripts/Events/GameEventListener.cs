using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Clase que se "suscribe" a un evento de un Scriptable Object y le asocia las funciones que responder�n a dicho evento (v�a editor)
/// @author Ra�l �ngel Lago Castro
/// </summary>
public class GameEventListener : MonoBehaviour
{
    [SerializeField]
    private SO_GameEvent Event;
    [SerializeField]
    private UnityEvent Response; //Unity Event con la respuesta

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised()
    {
        Response.Invoke();
    }
}
