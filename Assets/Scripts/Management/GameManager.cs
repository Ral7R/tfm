using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Script para gestionar los elementos no asociados a otras clases durante la partida y la respuesta a ciertos eventos
/// @author Ra�l �ngel Lago Castro
/// </summary>
public class GameManager : MonoBehaviour
{
    [SerializeField]
    private Text txtTargets;
    [SerializeField]
    private float waitingToLoadExit = 1f;
    [SerializeField]
    private float waitingToLoadEnd = 2f;
    [SerializeField]
    private Animator fadeAnim;

    private int score = 0;
    private DefaultInputActionAsset iaa;

    void Awake()
    {
        iaa = new DefaultInputActionAsset();

        iaa.Gameplay.Exit.performed += _ => Exit();
    }

    void Start()
    {
        score = GameObject.FindGameObjectsWithTag("Target").Length;
        txtTargets.text = score.ToString();
    }

    void OnEnable()
    {
        iaa.Enable();
    }

    void OnDisable()
    {
        iaa.Disable();
    }

    private void Exit()
    {
        StartCoroutine(LoadSceneAfterSeconds(0, waitingToLoadExit));
    }

    //Se llamar� a esta funci�n como respuesta a que una diana sea golpeada
    public void OnTargetHitted()
    {
        --score;
        txtTargets.text = score.ToString();

        if (score <= 0) StartCoroutine(LoadSceneAfterSeconds(2, waitingToLoadEnd));
    }

    private IEnumerator LoadSceneAfterSeconds(int index, float seconds)
    {
        fadeAnim.SetTrigger("FadeIn");
        yield return new WaitForSeconds(seconds);
        SceneManager.LoadScene(index);
    }
}
