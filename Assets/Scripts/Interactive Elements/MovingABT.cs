using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase para los objetos afectados por las mec�nicas de manipulaci�n temporal
/// y que incluye comportamineto de movimineto siguiendo un camino personalizable
/// @author Ra�l �ngel Lago Castro
/// </summary>
public class MovingABT : AffectedByTime
{
    [SerializeField]
    private List<Vector3> waypoints;
    [SerializeField]
    private bool isCircular    = false;
    [SerializeField]
    private bool inReverse     = true;
    [SerializeField]
    private float speed        = 3f;

    private int index          = 0;
    private int lastIndex      = 0;
    private bool lastInReverse = false;
    private Vector3 directionOfTravel;

    protected override void Awake()
    {
        base.Awake();
    }

    void Update()
    {
        if (stopped) return;

        if (waypoints[index] != null)
        {
            Move();
        }
    }

    public override void Interact()
    {
        base.Interact();
    }

    public override void SaveState()
    {
        lastIndex     = index;
        lastInReverse = inReverse;
        base.SaveState();
    }

    public override void LoadState()
    {
        index     = lastIndex;
        inReverse = lastInReverse;
        base.LoadState();
    }

    private void Move()
    {
        Vector3 currentPosition = this.transform.position;
        Vector3 targetPosition = waypoints[index];

        if (Vector3.Distance(currentPosition, targetPosition) > .1f)
        {
            directionOfTravel = targetPosition - currentPosition;
            directionOfTravel.Normalize();

            this.transform.Translate
            (
                directionOfTravel.x * speed * Time.deltaTime,
                directionOfTravel.y * speed * Time.deltaTime,
                directionOfTravel.z * speed * Time.deltaTime,
                Space.World
            );
        }
        else
        {
            NextWaypoint();
        }
    }

    private void NextWaypoint()
    {
        if (isCircular)
        {

            if (!inReverse)
            {
                index = (index + 1 >= waypoints.Count) ? 0 : index + 1;
            }
            else
            {
                index = (index == 0) ? waypoints.Count - 1 : index - 1;
            }

        }
        else
        {
            if ((!inReverse && index + 1 >= waypoints.Count) || (inReverse && index == 0))
            {
                inReverse = !inReverse;
            }

            index = (!inReverse) ? index + 1 : index - 1;
        }
    }

    void OnDrawGizmos()
    {
        if (this.waypoints == null || this.waypoints.Count == 0) return;

        Gizmos.color = Color.red;
        Gizmos.DrawLine(this.transform.position, this.waypoints[0]);

        for (int i = 0; i < this.waypoints.Count; i++)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(this.waypoints[i], 0.2f);

            if ((i + 1) < this.waypoints.Count)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(this.waypoints[i], this.waypoints[i + 1]);
            }
        }
    }
}
