using UnityEngine;

/// <summary>
/// Script que aplica un impulso por velocidad a cualquier objeto con RigidBody que entre en el trigger
/// @author Ra�l �ngel Lago Castro
/// </summary>
public class OnAnyTriggerImpulse : MonoBehaviour
{
    [SerializeField]
    private float velocityImpulse = 25f;

    private void OnTriggerEnter(Collider other)
    {
        Rigidbody rb = other.GetComponent<Rigidbody>();
        rb.velocity = new Vector3(rb.velocity.x, velocityImpulse, rb.velocity.z);
    }
}
